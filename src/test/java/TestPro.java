import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import org.junit.Test;

import static com.codeborne.selenide.Selenide.*;

public class TestPro {

    @Test
    public void login() {
        Configuration.timeout = 11000;
        open("https://www.olx.kz/");
        $("#topLoginLink").click();
        $("#userEmail").setValue("biialievi@gmail.com");
        $("#userPass").setValue("1122!Q2w");
        $$("#se_userLogin").findBy(Condition.text("Войти")).scrollTo();
        $$("#se_userLogin").findBy(Condition.text("Войти")).click();

        $x("//div[text()='biialievi']"); //login name
        $x("//h3[@data-testid='header-title']").should(Condition.text("Ваши объявления")); //page title
        $(".css-itibgd").should(Condition.text("Всего объявлений: 0")); //request count - 0
    }
}